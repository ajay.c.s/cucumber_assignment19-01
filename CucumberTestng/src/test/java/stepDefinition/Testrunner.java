package stepDefinition;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features = "src/test/resources/features", 
glue = {"stepDefinition"},plugin = { "pretty", "html:target/cucumber-reportss" },
monochrome = true)



public class Testrunner extends AbstractTestNGCucumberTests{

}



