Feature: Login feature with available credential in the website

Scenario Outline: Verify the user login with the credential login functionality
Given Open the browser
Then homepage of website is displayed
When User enter credentail to login <USERNAME> <PASSWORD>
And Click on the login button
Then Login successfully should be displayed
And Click on the Logout button
Then User should taken to homepage 
And close the browser

 Examples:
 | USERNAME | PASSWORD |
 | "ajaychandru12@gmail.com" | "Ajay@1999" |
 | "ajaychandru11@gmail.com" | "Ajay@1999" |
