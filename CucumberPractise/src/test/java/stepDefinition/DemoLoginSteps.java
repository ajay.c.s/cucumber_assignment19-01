package stepDefinition;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class DemoLoginSteps {
	static WebDriver driver = null;
@Given("Open the browser")
public void User_is_on_home_page() {
	System.setProperty("webdriver.chrome.driver", "/home/ajay/chromedriver_linux64/chromedriver");
    driver = new ChromeDriver();
	driver.get("https://demowebshop.tricentis.com/");
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
	
}
@Then("homepage of website is displayed")
public void homepage_of_website_is_displayed() {
	
	System.out.println("home page is displayed");
	
}
@When("User enter credentail to login")
public void user_enter_credential_to_login() {
	driver.findElement(By.xpath("//a[contains(text(),'Log in')]")).click();
	driver.findElement(By.id("Email")).sendKeys("ajaychandru12@gmail.com");
	driver.findElement(By.id("Password")).sendKeys("Ajay@1999");
	
}
@When("Click on the login button")
public void click_on_the_login_button() {
	driver.findElement(By.xpath("//input[@value='Log in']")).click();
}
@Then("Login successfully should be displayed")
public void login_successfully_should_be_displayed() {
	
	System.out.println("login succesful");
}
@Then("Click on the Logout button")
public void click_on_the_logout_button() {
	driver.findElement(By.linkText("Log out")).click();
}
@Then("User should taken to homepage")
public void user_should_taken_to_homepage() {
	System.out.println("homepage is displayed");
}
@Then("close the browser")
public void close_the_browser() {
	driver.quit();
}
}







